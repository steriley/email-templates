const browserSync = require('browser-sync');
const fs = require('fs');
const gulp = require('gulp');
const inlineCss = require('gulp-inline-css');
const mustache = require('gulp-mustache');
const sass = require('gulp-sass');

const output = 'output';
const templates = 'templates';
const data = 'data';

gulp.task('serve', ['sass', 'mustache'], () => {

  browserSync.init({
    server: {
      baseDir: output,
      index: 'newsletter.html'
    }
  });

  gulp.watch(data + '/*.json', ['mustache']);
  gulp.watch(templates + '/*.scss', ['sass','inlineCss']);
  gulp.watch(templates + '/*.mustache', ['mustache','inlineCss']);
  gulp.watch(output + '/newsletter.html').on('change', browserSync.reload);
});

gulp.task('sass', () => {
  return gulp.src(templates + '/*.scss')
    .pipe(sass())
    .pipe(gulp.dest(output));
});

gulp.task('mustache', () => {
  const content = fs.readFileSync(data + '/news.json');
  const templData = JSON.parse(content);
  return gulp.src(templates + '/newsletter.mustache')
    .pipe(mustache(templData, {extension: '.html'}))
    .pipe(gulp.dest(output + '/'));
})

gulp.task('inlineCss', ['sass', 'mustache'], () => {
  return gulp.src(output + '/*.html')
    .pipe(inlineCss({
      applyLinkTags: true,
      applyTableAttributes: true,
      removeLinkTags: true,
      removeHtmlSelectors: true
    }))
   .pipe(gulp.dest(output + '/'));
});

gulp.task('default', ['serve']);
